# Practice Gulp

This is Simple practice for Gulp

## Install

npm install

## Usage

npm run dev

# GULP

Task bundler[:(gulp 링크)](https://gulpjs.com/)

## API

- src()
- dest()
- symlink()
- lastRun()
- series()
- parallel()
- watch()
- task()
- registry()
- tree()
- Vinyl
- Vinyl.isVinyl()
- Vinyl.isCustomProp()

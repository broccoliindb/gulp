import gulp from "gulp"
import gulpPug from "gulp-pug"
import del from "del"
// import ws from "gulp-webserver"
import connect from "gulp-connect"
import gulpImage from "gulp-image"
import sass from "gulp-sass"
import autoPrefixer from "gulp-autoprefixer"
import csso from "gulp-csso"
import gulpBro from "gulp-bro"
import babelify from "babelify"
import uglifyify from "uglifyify"
import gulpOpen from "gulp-open"
import ghPages from "gulp-gh-pages-latest"

sass.compiler = require("node-sass")


const routes = {
  pug: {
    watch: "src/**/*.pug",
    src: "src/*.pug",
    dest: "build",
    index: "build/index.html"
  },
  img: {
    watch: "src/img/*",
    src: "src/img/*",
    dest: "build/img"
  },
  scss: {
    watch: "src/scss/**/*.scss",
    src: "src/scss/style.scss",
    dest: "build/css"
  },
  js: {
    watch: "src/js/**/*.js",
    src: "src/js/main.js",
    dest: "build/js"
  }
}

const pug = () =>
  gulp
    .src(routes.pug.src)
    .pipe(gulpPug())
    .pipe(gulp.dest(routes.pug.dest))
    .pipe(connect.reload())

const clean = () => del(["build"])

// const webserver = () => gulp.src("build").pipe(ws({
//   livereload: true,
//   open: true
// }))

const open = () => {
  let option = {
    uri: 'http://localhost:8001'
  }
  gulp.src(routes.pug.index)
    .pipe(gulpOpen(option))
  return Promise.resolve()
}

const webserver = () => {
  console.log('webserver')
  connect.server({
    root: "build",
    livereload: true,
    port: 8001
  })
  return Promise.resolve()
}

const img = () => {
  gulp.src(routes.img.src)
    .pipe(gulpImage())
    .pipe(gulp.dest(routes.img.dest))
    .pipe(connect.reload())

  return Promise.resolve()
}

const styles = () => {
  gulp.src(routes.scss.src)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoPrefixer())
    .pipe(csso())
    .pipe(gulp.dest(routes.scss.dest))
    .pipe(connect.reload())
  return Promise.resolve()
}

const js = () => {
  gulp.src(routes.js.src)
    .pipe(gulpBro({
      transform: [
        babelify.configure({ presets: ["@babel/preset-env"] }),
        ['uglifyify', { global: true }]
      ]
    }))
    .pipe(gulp.dest(routes.js.dest))
    .pipe(connect.reload())
  return Promise.resolve()
}


const detectChange = () => {
  console.log('watched')
  gulp.watch(routes.pug.watch, pug)
  gulp.watch(routes.img.watch, img)
  gulp.watch(routes.scss.watch, styles)
  gulp.watch(routes.js.watch, js)
  return Promise.resolve()
}

const gh = () => {
  gulp.src("build/**/*")
    .pipe(ghPages())
  console.log('deployed')
  return Promise.resolve()
}

const prepare = gulp.series([clean])

const assets = gulp.parallel([pug, img, styles, js])

const live = gulp.series([open, webserver])

const watch = gulp.series([detectChange])

const ghDeploy = gulp.series([gh])

export const build = gulp.series([prepare, assets])

export const dev = gulp.series([build, live, watch])

export const deploy = gulp.series([build, ghDeploy])

